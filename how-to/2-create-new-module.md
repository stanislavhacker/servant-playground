### 2. Creating new project

At the beginning we need to create a main [Servant][1] module, that is wrapper for others submodule.
 To do this run: 
 
 > `npm run servant -- --init`
 
This will run Servant's init module routine. Fill questions and try to fill these options, like on image
 bellow. You can choose also if you want to use **eslint** or **prettier** for validation but not necessary
 for this playground tutorial. This tutorial is used options from image, but you can fill your own :\) 
 
![ProjectInitAnswers][img1]

This will create [root `servant.json`][root/servant.json] with [root `package.json`][root/package.json] and finally 
 [root `tsconfig.json`][root/tsconfig.json]. There will be also `tests` and `src` directories, but we can delete it
 because we don't need them.
 
Now we have ancestor project. This project is used as wrapper for others modules, so we don't need any code here.
 All settings that are in servant.json will be used in submodules if we don't specify it. Now we can [continue to next chapter][ad3] 
 and try to add new submodule into project and create first code with [Servant][1]! 

 [img1]: ./assets/2a-ProjectInitAnswers.PNG
 
 [root/servant.json]: ../servant.json
 [root/package.json]: ../package.json
 [root/tsconfig.json]: ../tsconfig.json

 [1]: https://gitlab.com/stanislavhacker/servant
 [2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
 [3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
 [4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
 
 [ad3]: ./3-create-new-submodule.md