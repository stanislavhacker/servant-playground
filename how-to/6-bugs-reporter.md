### 6. Bugs reporter

Ok. Now we have browser module, nodejs module, all tests and we known how to debug your code. If you create
 a project with servant, make it bigger and publish it, you want to track bugs repair and count of repaired 
 bugs.
 
[Servant][1] can help with tracking all repaired and tested bugs. We need update our main `servant.json`
 and add some issue property.

**Add to content of `./servant.json`**
 
```json
  {
    ...
    "issues": {
      "#([0-9]{1,})": "http://issue-tracker-old.com/id/$0",
      "B-([0-9]{1,})": "http://my.youtrack.com/id/$0"     
    }
  }
```

We are added paters for matching and finding bugs in test name. This will cause that [Servant][1]
 will reports all matched and found tests for bugs. We are now add some tests into our tests files.
 
**Content of `todo.view/tests/basic.ts`**
 
```typescript
import * as app from "../src/index";

describe("app", () => {

	it("element create", () => {
		const el = app.init();
		expect(el.tagName.toLowerCase()).toBe("div");
		expect(el.textContent).toBe("Hello Servant!");
	});

	it("element create, test for very old bug #2", () => {
		const el = app.init();
		expect(el.tagName.toLowerCase()).toBe("div");
		expect(el.textContent).toBe("Hello Servant!");
	});

	it("B-2854: element create, test", () => {
		const el = app.init();
		expect(el.tagName.toLowerCase()).toBe("div");
		expect(el.textContent).toBe("Hello Servant!");
	});

});
```

**Content of `todo.server/tests/todo.ts`**

```typescript
import * as todo from "../src/index";

describe("check todo stats", () => {

	it("check on empty array", () => {
		const stats = todo.todoStats([]);

		expect(stats.solve).toBe(0);
		expect(stats.done).toBe(0);
	});

	it("check on filled array for user story #5", () => {
		const stats = todo.todoStats([
			{ name: "Buy tickets", description: "Buy tickets for Captain Marvel", done: false },
			{ name: "Make letter", description: "Make motivation letter for new job", done: false },
			{ name: "Try new game", description: "Install and try to play new game", done: true },
		]);

		expect(stats.solve).toBe(2);
		expect(stats.done).toBe(1);
	});

});
```

We are done and it's time to run tests!

 > `npm run servant -- tests` 
 
![ServantBugReport][img1]

[Servant][1] will report list of all founded bugs and count of tests that are used for check for regression.
 And it's super easy, some failed in bugs? You have a regression! Whole report can be also exported and
 save into file. Just run command with reporter.
 
 > `npm run servant -- tests --report "issues"`

![ServantBugReportSaveReport][img2]

[Servant][1] create file in `./temp/issues.txt` and this file will have content bellow. 
  
```txt
ID	P	F	E	P	URL
#5	1	0	0	0	http://issue-tracker-old.com/id/#5
#2	1	0	0	0	http://issue-tracker-old.com/id/#2
B-2854	1	0	0	0	http://my.youtrack.com/id/B-2854
```

Now we known how can [Servant][1] help you with bugs and we can [continue to next chapter][ad7]
 and look at package and production in [Servant][1]! 
 
 [img1]: ./assets/6a-ServantBugReport.PNG
 [img2]: ./assets/6b-ServantBugReportSaveFIle.PNG

 [1]: https://gitlab.com/stanislavhacker/servant
 [2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
 [3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
 [4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
 
 [ad7]: ./7-package-and-production.md