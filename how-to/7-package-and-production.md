### 7. Package and production

The last chapter is pretty simple. We have all what you need and now we want to create package and publish
 our todo app into repository. For this cases there is command `publish`. Fo testing purpouse we can run 
 command bellow and create finally package. This package is used for check if contains everything that you
 need. More [about publish command you can read here][5]
 
 > `npm run servant -- clean build publish`
 
This command will clean, build and run publish routine. But it's not really publish your package, just
 create `.tgz` package in directory of each module.
 
![ServantPublishCommand][img1]

![ServantCreatedPackages][img2]

We can look inside package and check content. 
 
     .
     ├── dist
     │   ├── src
     │   │   └── index.d.ts
     │   └── index.js
     └── package.json
 
[Servant][1] with NPM will add automatically this dist files into your package. If we want to add another
 files we must change `package.json` `files` property.
 
Ok now we have tested that package is ok. [Servant][1] automatically increment version before publish
 so we need to rever this version back to 1.0.0 and run publish again. But now with `--production` flag.
 
 > `npm run servant -- clean build publish --production`
 
This will create packages and publish results into defined repository. It's recommended to use 
 `--freeze` option with publish. You [can read about `--freeze` here][5].
 
And this is also end of [Servant][1] playground tutorial!
  
 [img1]: ./assets/7a-ServantPublishCommand.PNG
 [img2]: ./assets/7b-ServantNpmPackages.PNG

 [1]: https://gitlab.com/stanislavhacker/servant
 [2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
 [3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
 [4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
 [5]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.publish.md