### 3. Creating new sub-module

Now we can add modules into your main [Servant][1] project. This can be done similar as in previous step, but
 we need make some changes in our answers.
 
 > `npm run servant -- --init`
 
This will run Servant's init module routine. Fill questions and try fill these options, like on image
 bellow. This tutorial is used to use these options, but you can fill your own :\)
 
![ProjectSubmoduleAnswers][img1]

This will create new module that is used for view some todo items in web browser. This structure can
 be view on screen bellow. This module can be added to your version system and you can commit these defaults
 structure. You can see that TypeScript files created by servant are empty. You must write your own code
 here :\)
 
![ProjectSubmoduleStructure][img2]

Now you have your first [Servant][1] submodule! You can now implement module content and write some tests.
 But before that, we need to build our project and start watcher. Run comment bellow.
 
 > `npm run servant -- clean build --watch`
 
This will clean and build project and start watcher. Watcher will be watching for changes in project files
 and re-build project every changes that you make into file. Now you have all setup and you can write some 
 code. Servant create `todo.view/src/index.ts` and entry file for tests `todo.view/tests/index.ts`. We use 
 some predefined code and create one another file for tests. Create `todo.view/tests/basic.ts` manually.
 
 > **INFO**
 >
 > All these files that are created by Servant and file that we created manually are used to be
 > part of your git repo. Add this files into your version control system (now only GIT is supported).
 
**Content of `todo.view/src/index.ts`**

```typescript
export function init(): Element {
	const element = document.createElement("div");

	element.textContent = "Hello Servant!";
	return element;
}
```

**Content of `todo.view/tests/index.ts`**

```typescript
import "./basic.ts";
```

**Content of `todo.view/tests/basic.ts`**

```typescript
import * as app from "../src/index";

describe("app", () => {

	it("element create", () => {
		const el = app.init();
		expect(el.tagName.toLowerCase()).toBe("div");
		expect(el.textContent).toBe("Hello Servant!");
	});

});
```


#### Build

Ok now, we have our first web browser project with tests! So we can try to build it! [Servant][1] will watching your changes
 so build is made automatically, but we can try to run it manually. The best way is to run command bellow:

 > `npm run servant -- clean build`
 
![ServantCleanBuildRun][img3]
 
This command remove all necessary files, temp files and generated files and after that run build. Build make files
 into defined output directory. In this case is `./dist` and main file will be `index.js`. There will be
 also `d.ts` files for your module.
 
#### Tests

The next step that we can do is running tests. Because we prepare simple test before, we can easily run command to run
 all tests for all modules.
 
 > `npm run servant -- tests` 
 
![ServantTestsRun][img4]

As we can see, [Servant][1] run test for all modules. We have only one now, but there will be list of all modules and count
 of passed, skipped and failed tests. [Servant][1] running tests based on the specified target on `servant.json` for module.
 If we specified module for _web_ [Servant][1] will be running this tests in browser (for now, it's only Chrome). If we choose
 _node_ or _node-cli_ [Servant][1] will be running this tests in node environment.
 
[Servant][1] use **Jasmine** for tests and we can mark test as skipped or pending. Now we change test `todo.view/tests/basic.ts` file
 and we added some pending and failing tests.
 
**Content of `todo.view/tests/basic.ts`**

```typescript
import * as app from "../src/index";

describe("app", () => {

	it("element create", () => {
		const el = app.init();
		expect(el.tagName.toLowerCase()).toBe("div");
		expect(el.textContent).toBe("Hello Servant!");
	});

	xit("check second line of text 1", () => {
		const el = app.init();
		expect(el.tagName.toLowerCase()).toBe("div");
		expect(el.textContent).toBe("Hello Servant! My name is Stanley.");
	});

	it("check second line of text 2", () => {
		pending("This is not implemented yet.");
		const el = app.init();
		expect(el.tagName.toLowerCase()).toBe("div");
		expect(el.textContent).toBe("Hello Servant! My name is Stanley.");
	});

	it("check if is span", () => {
		const el = app.init();
		expect(el.tagName.toLowerCase()).toBe("span");
	});

});
```

And now we run tests command again.

 > `npm run servant -- tests` 
 
![ServantTestsRunFail][img5]

As we can see on this console output, [Servant][1] runs tests, but there are some errors. It's look like that one test
 failed (is marked as red and we can see expectation results) and two tests are marked as pending. So now we can see how
 [Servant][1] handling pending and failed tests. Ok now we can revert changes that we made into `todo.view/tests/basic.ts`
 and make tests green again.
 
Now we can [continue to next chapter][ad4] and try to preview this module and make some debugging session of code and
 tests in [Servant][1]! 

 
 [img1]: ./assets/3a-ProjectSubmoduleInit.PNG
 [img2]: ./assets/3b-NewModuleStructure.PNG
 [img3]: ./assets/3c-ServantCleanBuildRun.PNG
 [img4]: ./assets/3d-ServantTestsRun.PNG
 [img5]: ./assets/3e-ServantTestsRunFail.PNG

 [1]: https://gitlab.com/stanislavhacker/servant
 [2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
 [3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
 [4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
 
 [ad4]: ./4-preview-and-debugging.md