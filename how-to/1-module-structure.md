### 1. Project structure

This is describe the base module structure. Every module can contains these files. It's recommended. But
 you can skip some files. Read more about files content and meaning.
 
![ProjectStructure][img1]

#### Contained files

     ├── .gitignore
     
This is file for adding ignores files for GIT version system. Can be here if you use git. Using git is recommended
 for right work of Servant.
 
     ├── CHANGELOG.md
     
Changelog file that must be in every project. No it's not neccessary but it's good. The format is based on 
 [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to 
 [Semantic Versioning](https://semver.org/spec/v2.0.0.html). It's good for open source projects.

     ├── CONTRIBUTING.md

Files where are described rules for contributing to your project. THis is an standard example that
 you can use in your project or just delete it :) but it's goo for open source projects.

     ├── README.md

README.md file is must have. A README is often the first item a visitor will see when visiting your repository. 
 README files typically include information on:
                             
 - What the project does
 - Why the project is useful
 - How users can get started with the project
 - Where users can get help with your project
 - Who maintains and contributes to the project
 - If you put your README file in your repository's root, docs, or hidden .github directory, GitHub will recognize and automatically surface your README to repository visitors.

It's recommended to have README file in your project!
     
     ├── LICENSE


Public repositories on GitHub are often used to share open source software. For your repository to truly be open source, you'll need to license it so that others are free to use, 
 change, and distribute the software.
     
     ├── logo.png

Logo of your project. This logo is used by gitlab. No special meaning for your project :D
     
     ├── package.json

This file can contain a lot of meta-data about your project. But mostly it will be used for 
 two things:

 - Managing dependencies of your project
 - Scripts, that helps in generating builds, running tests and other stuff in 
 regards to your project
 
Servant need [package.json][0] for right working! If you use commands `servant --init`, Servant will create
 this file in project automatically.
  
     ├── servant.json

Main configuration file for Servant. It's used for modules and package definition. Structure of file
 is described below on Servant project doc for [servant.json][2].  If you use commands `servant --init`,
 Servant will create this file in project automatically.
 
     ├── tsconfig.json
     
The presence of a tsconfig.json file in a directory indicates that the directory is the root of a TypeScript 
 project. The tsconfig.json file specifies the root files and the compiler options required to compile the 
 project. This file is not necessary because [Servant][1] is able to work without tsconfig.json.
 
Now you known about basic project structure. You can [continue to next chapter][ad2] and try to add new 
 module into project.

 [logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
 [img1]: ./assets/1a-ProjectStructure.PNG
 
 [0]: https://docs.npmjs.com/files/package.json
 [1]: https://gitlab.com/stanislavhacker/servant
 [2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
 [3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
 [4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
 
 [ad2]: ./2-create-new-module.md