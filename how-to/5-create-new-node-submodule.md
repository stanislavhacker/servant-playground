### 5. Create new node submodule

Now we can add another module into your main [Servant][1] project. We now known how to add module, so we
 can add new one, but now it will be intended for nodejs server.
 
 > `npm run servant -- --init`
 
This will run Servant's init module routine. Fill questions and try fill these options, like on image
 bellow. This tutorial is used to use these options, but you can fill your own :\)
 
![ProjectAddNodeModule][img1]

This will create new module that is used as server for todo items. It's used to run in nodejs. This module 
 can be added to your version system and you can commit these defaults structure. You can see that TypeScript 
 files created by servant are empty. You must write your own code here :\)
 
Now you have your second [Servant][1] submodule! You can now implement module content and write some tests as in previous
 chapter for creating module. But now we must write nodejs code and nodejs tests.
 
**Content of `todo.server/src/index.ts`**

```typescript
export interface ToDo {
	description: string,
	name: string,
	done: boolean
}

export interface ToDoStats {
	done: number;
	solve: number;
}

export function todoStats(todo: Array<ToDo>): ToDoStats {
	const stats: ToDoStats = {
		done: 0,
		solve: 0
	};

	todo.forEach((td) => {
		if (td.done) {
			stats.done++;
		} else {
			stats.solve++;
		}
	});

	return stats;
}
```

We have module that doing some statistics froml array of ToDo items. Now we want to write some tests. Create file 
 `todo.server/tests/todo.ts` and let's write test.
 
**Content of `todo.server/tests/todo.ts`**

```typescript
import * as todo from "../src/index";

describe("check todo stats", () => {

	it("check on empty array", () => {
		const stats = todo.todoStats([]);

		expect(stats.solve).toBe(0);
		expect(stats.done).toBe(0);
	});

	it("check on filled array", () => {
		const stats = todo.todoStats([
			{ name: "Buy tickets", description: "Buy tickets for Captain Marvel", done: false },
			{ name: "Make letter", description: "Make motivation letter for new job", done: false },
			{ name: "Try new game", description: "Install and try to play new game", done: true },
		]);

		expect(stats.solve).toBe(2);
		expect(stats.done).toBe(1);
	});

});
```

We created tests for our node module and now we can run tests. Running tests we known from previous chapter
 and it's super easy. Just run command bellow.
 
 > `npm run servant -- tests` 
 
 ![ServantTestsRun][img2]
 
Servant run tests for all modules so we can see here two modules info and all passed or failed tests. This is same
 like in previous chapter. For node tests there will be passed, skipped and failed tests.
 
Now we can [continue to next chapter][ad6] and look at the bugs reports in [Servant][1]! 

 [img1]: ./assets/5a-ProjectAddNodeModule.PNG
 [img2]: ./assets/5b-ServantTestsRun.PNG

 [1]: https://gitlab.com/stanislavhacker/servant
 [2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
 [3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
 [4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
 
 [ad6]: ./6-bugs-reporter.md