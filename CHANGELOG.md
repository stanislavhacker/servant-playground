# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
 - Changelog file
 - README 
 - Other repo files for init
 - package.json, servant.json

[Unreleased]: https://gitlab.com/stanislavhacker/servant-playground/compare/0.1.1...master
