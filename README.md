![Servant][logo] Servant playground
======
Quick references: **[Servant][1]**, **[Command line][4]**, **[Node API][3]**, **[servant.json][2]**

## What is it?
Playground is testing module and example of using [Servant][1] build tool. This repository shows the 
 best approach for using [Servant][1] as good tool for developers. It's shows the most standard usage. 
 You can testing [Servant][1] here, observe the behaviour or just playing with it.
 
### Let's get started!

At the start, after you clone this repo, run command for install [Servant][1] in current folder.

 > `npm install`
 
Now [Servant][1] is installed and you can use it through npm run script command.

 > `npm run script servant`
 
If this command show you same result as it on screen bellow, you are done.

![Servant run test][0a] 

Now you can move to [next step][ad1].

 1. **step**: [Basic module structure][ad1]
 1. **step**: [Creating new module][ad2]
 1. **step**: [Creating new submodule][ad3]
 1. **step**: [Preview and debugging][ad4]
 1. **step**: [Create new node submodule][ad5]
 1. **step**: [Bugs reporter][ad6]
 1. **step**: [Package and production][ad7]

### Donate me 😉

| QR | Paypal |
| ------ | ------ |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |

 
 
 [logo]: https://gitlab.com/stanislavhacker/servant/raw/master/logo.png
 [0a]: ./how-to/assets/0a-ServantRunTest.PNG
 
 [1]: https://gitlab.com/stanislavhacker/servant
 [2]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.json.md
 [3]: https://gitlab.com/stanislavhacker/servant/blob/master/servant/doc/servant.nodejs.md
 [4]: https://gitlab.com/stanislavhacker/servant/blob/master/servant-cli/doc/servant.clia.md
 
 [ad1]: ./how-to/1-module-structure.md
 [ad2]: ./how-to/2-create-new-module.md
 [ad3]: ./how-to/3-create-new-submodule.md
 [ad4]: ./how-to/4-preview-and-debugging.md
 [ad5]: ./how-to/5-create-new-node-submodule.md
 [ad6]: ./how-to/6-bugs-reporter.md
 [ad7]: ./how-to/7-package-and-production.md